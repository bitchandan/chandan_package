# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'chandan_gui.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName(_fromUtf8("Form"))
        Form.resize(662, 269)
        self.connect_joints = QtGui.QPushButton(Form)
        self.connect_joints.setGeometry(QtCore.QRect(10, 30, 371, 51))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setItalic(True)
        font.setWeight(75)
        self.connect_joints.setFont(font)
        self.connect_joints.setObjectName(_fromUtf8("connect_joints"))
        self.connect_arduino = QtGui.QPushButton(Form)
        self.connect_arduino.setGeometry(QtCore.QRect(440, 60, 191, 51))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setItalic(True)
        font.setWeight(75)
        self.connect_arduino.setFont(font)
        self.connect_arduino.setObjectName(_fromUtf8("connect_arduino"))
        self.button_reach = QtGui.QPushButton(Form)
        self.button_reach.setGeometry(QtCore.QRect(440, 120, 191, 51))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setItalic(True)
        font.setWeight(75)
        self.button_reach.setFont(font)
        self.button_reach.setObjectName(_fromUtf8("button_reach"))
        self.lbl_link1 = QtGui.QLabel(Form)
        self.lbl_link1.setGeometry(QtCore.QRect(29, 90, 51, 20))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setItalic(False)
        font.setWeight(75)
        self.lbl_link1.setFont(font)
        self.lbl_link1.setObjectName(_fromUtf8("lbl_link1"))
        self.sldr_link1 = QtGui.QSlider(Form)
        self.sldr_link1.setGeometry(QtCore.QRect(80, 88, 201, 29))
        self.sldr_link1.setMinimum(-90)
        self.sldr_link1.setMaximum(90)
        self.sldr_link1.setProperty("value", 1)
        self.sldr_link1.setSliderPosition(1)
        self.sldr_link1.setOrientation(QtCore.Qt.Horizontal)
        self.sldr_link1.setObjectName(_fromUtf8("sldr_link1"))
        self.lbl_link2 = QtGui.QLabel(Form)
        self.lbl_link2.setGeometry(QtCore.QRect(29, 144, 51, 20))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.lbl_link2.setFont(font)
        self.lbl_link2.setObjectName(_fromUtf8("lbl_link2"))
        self.sldr_link2 = QtGui.QSlider(Form)
        self.sldr_link2.setGeometry(QtCore.QRect(80, 142, 201, 29))
        self.sldr_link2.setMinimum(-90)
        self.sldr_link2.setMaximum(90)
        self.sldr_link2.setOrientation(QtCore.Qt.Horizontal)
        self.sldr_link2.setObjectName(_fromUtf8("sldr_link2"))
        self.lbl_link3 = QtGui.QLabel(Form)
        self.lbl_link3.setGeometry(QtCore.QRect(30, 198, 51, 20))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setItalic(False)
        font.setWeight(75)
        self.lbl_link3.setFont(font)
        self.lbl_link3.setObjectName(_fromUtf8("lbl_link3"))
        self.sldr_link3 = QtGui.QSlider(Form)
        self.sldr_link3.setGeometry(QtCore.QRect(80, 196, 201, 29))
        self.sldr_link3.setMinimum(-90)
        self.sldr_link3.setMaximum(90)
        self.sldr_link3.setOrientation(QtCore.Qt.Horizontal)
        self.sldr_link3.setObjectName(_fromUtf8("sldr_link3"))
        self.lbl_vallink1 = QtGui.QLabel(Form)
        self.lbl_vallink1.setGeometry(QtCore.QRect(310, 90, 50, 20))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setItalic(True)
        font.setWeight(75)
        self.lbl_vallink1.setFont(font)
        self.lbl_vallink1.setObjectName(_fromUtf8("lbl_vallink1"))
        self.lbl_vallink2 = QtGui.QLabel(Form)
        self.lbl_vallink2.setGeometry(QtCore.QRect(310, 144, 50, 20))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setItalic(True)
        font.setWeight(75)
        self.lbl_vallink2.setFont(font)
        self.lbl_vallink2.setObjectName(_fromUtf8("lbl_vallink2"))
        self.lbl_vallink3 = QtGui.QLabel(Form)
        self.lbl_vallink3.setGeometry(QtCore.QRect(310, 198, 50, 20))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setItalic(True)
        font.setWeight(75)
        self.lbl_vallink3.setFont(font)
        self.lbl_vallink3.setObjectName(_fromUtf8("lbl_vallink3"))

        self.retranslateUi(Form)
        QtCore.QObject.connect(self.sldr_link1, QtCore.SIGNAL(_fromUtf8("valueChanged(int)")), self.lbl_vallink1.setNum)
        QtCore.QObject.connect(self.sldr_link2, QtCore.SIGNAL(_fromUtf8("valueChanged(int)")), self.lbl_vallink2.setNum)
        QtCore.QObject.connect(self.sldr_link3, QtCore.SIGNAL(_fromUtf8("valueChanged(int)")), self.lbl_vallink3.setNum)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(_translate("Form", "Form", None))
        self.connect_joints.setText(_translate("Form", "Connect to ROS Joints", None))
        self.connect_arduino.setText(_translate("Form", "Connect Arduino", None))
        self.button_reach.setText(_translate("Form", "Reach Envelope", None))
        self.lbl_link1.setText(_translate("Form", "Link 1", None))
        self.lbl_link2.setText(_translate("Form", "Link 2", None))
        self.lbl_link3.setText(_translate("Form", "Link 3", None))
        self.lbl_vallink1.setText(_translate("Form", "0", None))
        self.lbl_vallink2.setText(_translate("Form", "0", None))
        self.lbl_vallink3.setText(_translate("Form", "0", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Form = QtGui.QWidget()
    ui = Ui_Form()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())

