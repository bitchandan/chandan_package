from PyQt4 import QtGui # Import the PyQt4 module we'll need
import sys # We need sys so that we can pass argv to QApplication
import rospy
from sensor_msgs.msg import JointState
from std_msgs.msg import Float32 #float32 because my arduino node msgs is in float
import chandan_gui # to import the gui

class GUI_class_chandan(QtGui.QMainWindow, chandan_gui.Ui_Form):
    
    joints = JointState()
    def __init__(self):
        super(self.__class__, self).__init__()
        self.setupUi(self) #setsup GUI

#-----------------------------------------------------------------------------------------------
        #button call  to connect to Joint state publisher
        self.connect_joints.clicked.connect (self.connect_to_publisher)
        #updating the joint values
        self.sldr_link1.valueChanged[int].connect(self.get_slider_1)
        self.sldr_link2.valueChanged[int].connect(self.get_slider_2)
        self.sldr_link3.valueChanged[int].connect(self.get_slider_3)

        self.joints.name = ['joint_1','joint_2','joint_3']
        self.value = [0.0,0.0,0.0] 
        self.joints.position = [self.value[0],self.value[1],self.value[2]]
        #button call back to listen to arduino
        self.connect_arduino.clicked.connect(self.listener)

        #button call back for reach envolope
        self.button_reach.clicked.connect (self.reach_plot)


    def connect_to_publisher(self):
        rospy.init_node('chandan_node', anonymous=True)
        self.jpub = rospy.Publisher('/joint_states',JointState, queue_size=10)
        
        print('----------------------connected to rviz---------------------- ');

    #getting slider values from GUI and convert it to radians
    def get_slider_1(self):
        self.value[0] = self.sldr_link1.value()/100.0*1.57
        #print (self.value[0])
        self.joints.position = [self.value[0],self.value[1],self.value[2]]

        print("j_1 = %.3f j_2 = %.3f j_3 = %.3f" %(self.value[0],self.value[1],self.value[2]))

        self.jpub.publish(self.joints)

    def get_slider_2(self):
        self.value[1] = self.sldr_link2.value()/100.0*1.57
        #print (self.value[1])
        self.joints.position = [self.value[0],self.value[1],self.value[2]]

        print("j_1 = %.3f j_2 = %.3f j_3 = %.3f" %(self.value[0],self.value[1],self.value[2]))

        self.jpub.publish(self.joints)

    def get_slider_3(self):
        self.value[2] = self.sldr_link3.value()/100.0*1.57
        #print (self.value[2])
        self.joints.position = [self.value[0],self.value[1],self.value[2]]

        print("j_1 = %.3f j_2 = %.3f j_3 = %.3f" %(self.value[0],self.value[1],self.value[2]))
        self.jpub.publish(self.joints)

#-----------------------------------------------------------------------------------------------
        

    #creating Subscriber for Arduino 
    def listener(self):
        #to listen to Arduino topics
        rospy.Subscriber('/x_position', Float32, self.callbackx)
        rospy.Subscriber('/y_position', Float32, self.callbacky)
        rospy.spin()

    def callbackx(self,data):
       
        self.datax = data.data
        self.mapd_x_data = convert(data.data,0,1024,-1.57,1.57)    
        #updating the joint position values
        self.value[0] = self.mapd_x_data 
        self.joints.position = [self.value[0],self.value[1],self.value[2]]

        self.jpub.publish(self.joints)
        #print(self.mapd_x_data)

    def callbacky(self,data):
       
        self.datay = data.data
        self.mapd_y_data = convert(data.data,0,1024,-1.57,1.57)
        #updating the joint on values
        self.value[2] = self.mapd_y_data 
        self.joints.position = [self.value[0],self.value[1],self.value[2]]

        self.jpub.publish(self.joints)

        print( "(arduino_x =   %.3f)-- (arduino_y   =   %.3f)" %(self.datax,self.datay))
        print( "(mapedx    =   %.3f)-- (mapedy      =   %.3f)"  %(self.mapd_x_data,self.mapd_y_data))


#-----------------------------------------------------------------------------------------------
        
    #def to plot reach envolope
    def reach_plot(self):
        print("button clicked")
        for i in range(-1.57,1.57,0.1):
            for j in range(-1.57,1.57,0.01):
                for k in range(-1.57,1.57,0.001):
                    self.joints.position = [i,j,k]
                    self.jpub.publish(self.joints)
                    print("j_1 = %.3f j_2 = %.3f j_3 = %.3f" %(i,j,k))

    #to iterate floating points in python we create a class to get points
def range(x,y,jump):
    while x<y:
        yield x
        x += jump

def convert(value, analog_val_min, analog_val_max, joint_min, joint_max):
        # to find the range
    analog = analog_val_max - analog_val_min
    joint = joint_max - joint_min
    #scaling
    valueScaled = float(value - analog_val_min) / float(analog)
        #return the value
    return joint_min + (valueScaled * joint)

#-----------------------------------------------------------------------------------------------

def main():
    app = QtGui.QApplication(sys.argv)  # A new instance of QApplication
    form = GUI_class_chandan()              
    form.show()                         # Show the form
    
    sys.exit(app.exec_())                         # and execute the app
    #rospy.spin()

if __name__ == '__main__':              # if we're running file directly and not importing it
    main()
                          # run the main function
#-----------------------------------------------------------------------------------------------